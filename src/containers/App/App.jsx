import React, {Component} from 'react';
import {Redirect, Route, Switch} from 'react-router-dom';

import Header from 'components/Header/Header';
import Sidebar from 'components/Sidebar/Sidebar';

import {style} from "variables/Variables.jsx";

import appRoutes from 'routes/app.jsx';
import client from "../../api/index";
import Login from "../../views/Login/Login";
import PrivateRoute from "../../views/AuthRoute/AuthRoute";

import {Launcher} from 'react-chat-window';
import {getMessages} from "../../operations";
import {authenticate} from "../../api";

class App extends Component {

    constructor(props) {
        super(props);
        this.componentDidMount = this.componentDidMount.bind(this);
        this.state = {
            _notificationSystem: null,
            messageList: [{
                author: "them",
                type: "text",
                data: {
                    text: "Hi there"
                }
            }, {
                author: "me",
                type: "text",
                data: {
                    text: "Hi"
                }
            }]
        };
        authenticate()
    }

    _onMessageWasSent(message) {
        this.setState({
            messageList: [...this.state.messageList, message]
        })
    }

    componentDidMount() {

        client.service('messages').on('created', (message) => {
            this.setState({
                messageList: [...this.state.messageList, {
                    author: 'them',
                    type: 'text',
                    data: {message}
                }]
            })
        });
        /*.catch(err=>alert(JSON.stringify(err)));*/
    }

    componentDidUpdate(e) {
        if (window.innerWidth < 993 && e.history.location.pathname !== e.location.pathname && document.documentElement.className.indexOf('nav-open') !== -1) {
            document.documentElement.classList.toggle('nav-open');
        }
    }

    render() {
        return (
            <div className="wrapper">
                {(this.props.location.pathname === "/login") ?
                    <Route
                        path="/login"
                        key="login"
                        render={routeProps =>
                            <Login
                                {...routeProps}
                            />}
                    /> :
                    <div>
                        <Sidebar {...this.props} />
                        <div id="main-panel" className="main-panel">
                            <Header {...this.props}/>
                            <Switch>
                                {
                                    appRoutes.map((prop, key) => {
                                        if (prop.redirect)
                                            return (
                                                <Redirect from={prop.path} to={prop.to} key={key}/>
                                            );
                                        return (
                                            <PrivateRoute path={prop.path}
                                                          component={prop.component}
                                                          key={key}/>
                                        );
                                    })
                                }
                            </Switch>
                        </div>
                    </div>
                }
                <Launcher
                    style={{zIndex: 9999}}
                    agentProfile={{
                        teamName: 'Meda Shi',
                    }}
                    onMessageWasSent={this._onMessageWasSent.bind(this)}
                    messageList={this.state.messageList}
                    showEmoji
                />
            </div>
        );
    }
}

export default App;
