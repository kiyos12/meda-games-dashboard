import client from "../api/index";

export const createQuestion = (question) => client.service('question').create(question);

export const updateQuestion = (question) => {
    return client.service('question').update(question._id, question)
};

export const addTag = (tag) => {
    return client.service('tags').create(tag)
};

export const createNews = (news) => client.service('news').create(news);

export const sendNotification = (notification) => {
    return fetch("https://fcm.googleapis.com/fcm/send",
        {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'key=AIzaSyAdyujsmNxXHe5XiJVSSJ024-9Jkit-tfY',
            },
            method: "POST",
            body: JSON.stringify(notification)
        })
};

export const getQuestion = (id) => client.service('question').find({
    query: {
        _id: id
    }
});

export const getCategories = () => client.service('category').find();

export const getTags = () => client.service('tags').find();

export const getMessages = (page) => client.service('messages').find();

export const patchUsers = () => client.service('users').patch(null, {won: 100}, {"avatar_emoji": "🍡"});

export const createCategory = (name) => client.service('category').create(name);

export const createGame = (game) => client.service('game').create(game);

export const updateGame = (game_id, game) => {
    return client.service('game').update(game_id, game)
};

export const getGames = (game) => client.service('game').find();

export const scheduleGame = (id) => {
    return removeScheduledGames().then(() => {
        return client.service('game').update(id, {
            $set: {
                scheduled: "true",
                started: "false",
                finished: "false"
            }
        })
    });
};

export const deleteGame = (id) => client.service('game').remove(id);

export const startGame = (id) => {
    removeScheduledGames().then(() => {
        removeStartedGames().then(() => {
            return client.service('game').update(id, {$set: {started: "true"}})
                .then(response => console.log(response))
        })
    })
};

const removeScheduledGames = () => client.service('game').patch(null, {
    scheduled: "false",
});

const removeStartedGames = () => client.service('game').patch(null, {
    started: "false",
});

export const getGame = (id) => client.service('game').get(id);

export const reOrderQuestions = (updatedQuestions, game_id) => client.service('game').update(game_id,
    {
        $set: {
            questions: updatedQuestions,
        }
    });

export const getQuestions = () => client.service('question').find();

export const getQuestionsByPage = (page) => client.service('question').find({
    query: {
        $skip: page
    }
});

export const getGamesByPage = (page) => client.service('game').find({
    query: {
        $skip: page
    }
});

export const getUsersByPage = (page) => client.service('users').find({
    query: {
        $skip: page
    }
});

let isAuthenticated = false;

export const setAuthenticated = (value) => {
    isAuthenticated = value;
};

export const getAuthenticated = () => {
    // alert(localStorage.getItem('feathers-jwt'))
    return localStorage.getItem('feathers-jwt') !== null
};

export const getUsers = () => client.service('users').find();

export const searchQuestions = (text) => client.service('question').find({
    query: {
        "question": {$search: text}
    }
});

export const getQuestionsByTags = (tags) => {
    return client.service('question').find({
        query: {
            "tags.text": {
                $in: tags
            }
        }
    })
};

export const getQuestionsByCategories = (categories) => {
    return client.service('question').find({
        query: {
            "categories.name": {
                $in: categories
            }
        }
    })
};

export const getQuestionsByStatus = (status) => {
    if(status == 'not_asked') {
        return client.service('question').find({
            query: {
                asked_games: {
                    $in: {
                        $eq: undefined
                    }
                }
            }
        })
    }
    else {
        return client.service('question').find({
            query: {
                asked_games: {
                    $in : {
                        $regex: "/^\d+$/"
                    }
                }
            }
        })
    }
};

export const deleteQuestions = (question) => client.service('question').remove(question);

export const createAds = (ads) => client.service('advertisement').create(ads);

export const updateAds = (ads_id, ads) => {
    return client.service('advertisement').update(ads_id, ads)
};

export const deleteAds = (ads) => client.service('advertisement').remove(ads);

export const getAds = (id) => client.service('advertisement').get(id);

export const getAdverts = () => client.service('ads').find();

export const getAdsByPage = (page) => client.service('advertisement').find({
    query: {
        $skip: page
    }
});


