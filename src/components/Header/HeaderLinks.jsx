import React, {Component} from 'react';
import {Button, MenuItem, Nav, NavDropdown, NavItem} from 'react-bootstrap';
import client from "../../api/index";
import {setAuthenticated} from "../../operations/index";


class HeaderLinks extends Component {

    seeQuestions = () => {
        if (typeof this.props.location !== "undefined") {
            if (this.props.location.pathname === "/question")
                return (
                    <Nav>
                        <NavItem eventkey={3} href="#">See Questions</NavItem>
                    </Nav>
                )
        }
    };

    render() {
        const notification = (
            <div>
                <i className="fa fa-globe">
                </i>
                <b className="caret">
                </b>
                <span className="notification">5</span>
                <p className="hidden-lg hidden-md">Notification</p>
            </div>
        );
        return (
            <div>
                <Nav>
                    <NavItem eventkey={1} href="#">
                        <i className="fa fa-dashboard"/>
                        <p className="hidden-lg hidden-md">Dashboard</p>
                    </NavItem>
                    <NavDropdown eventkey={2} title={notification} noCaret id="basic-nav-dropdown">
                        <MenuItem eventkey={2.1}>Notification 1</MenuItem>
                        <MenuItem eventkey={2.2}>Notification 2</MenuItem>
                        <MenuItem eventkey={2.3}>Notification 3</MenuItem>
                        <MenuItem eventkey={2.4}>Notification 4</MenuItem>
                        <MenuItem eventkey={2.5}>Another notifications</MenuItem>
                    </NavDropdown>
                    <NavItem eventkey={3} href="#">
                        <i className="fa fa-search">
                        </i>
                        <p className="hidden-lg hidden-md">Search</p>
                    </NavItem>
                </Nav>
                {this.seeQuestions()}

                <Nav pullRight>
                    <Button eventkey={3} href="#" onClick={() => {
                        client.logout();
                        setAuthenticated(false);
                        this.props.history.push('/login')
                    }}>Log out</Button>
                </Nav>
            </div>
        );
    }
}

export default HeaderLinks;
