import React, {Component} from 'react';
import {FormControl, FormGroup, InputGroup, Row} from 'react-bootstrap';

function FieldGroup({label, ...props}) {
    return (
        <FormGroup>
            <InputGroup>
                <InputGroup.Addon style={{backgroundColor: "#F1F1F1"}}>{label} </InputGroup.Addon>
                <FormControl {...props} />
            </InputGroup>
        </FormGroup>
    );
}

export class FormInputPrepend extends Component {
    render() {
        var row = [];
        for (var i = 0; i < this.props.ncols.length; i++) {
            row.push(
                <div key={i} className={this.props.ncols[i]}>
                    <FieldGroup
                        {...this.props.proprieties[i]}
                    />
                </div>
            );
        }
        return (
            <Row>
                {row}
            </Row>
        );
    }
}

export default FormInputPrepend;
