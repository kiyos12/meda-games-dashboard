import React, {Component} from 'react';
import {countDown} from "../utills/Time";

class CountDown extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cdd: ""
        }
    }

    componentDidMount() {
        countDown(this.props.dateFrom, (day, hour, minute, second) => {
            this.setState({
                cdd: day + "d " + hour + "h " + minute + "m " + second + "s"
            })
        })
    }

    render() {
        return (
            <div>
                <p>{this.state.cdd}</p>
            </div>
        );
    }
}

CountDown.propTypes = {};
CountDown.defaultProps = {};

export default CountDown;
