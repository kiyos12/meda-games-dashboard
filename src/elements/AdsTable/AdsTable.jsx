import React, { Component } from 'react';
import {Button, OverlayTrigger, Pagination, Table, Tooltip} from "react-bootstrap";
import { NavLink } from "react-router-dom";
import { getAdsByPage, deleteAds } from "../../operations/index";

class AdsTable extends Component {

    loadPage = (page) => {
        let skip = this.state.limit * (page - 1);
        getAdsByPage(skip).then(res => {
            this.setState({
                ads: res.data,
                active: page
            })
        }).catch(err => alert(err));
    };

    constructor(props) {
        super(props);
        this.state = {
            total: 0,
            skip: 0,
            limit: 0,
            ads: [],
            active: 1,
        }
    }

    componentWillReceiveProps(nextProps, nextState) {
        if (this.props.ads !== nextProps.ads) {
            this.setState({
                ads: nextProps.ads,
                total: nextProps.total,
                skip: nextProps.skip,
                limit: nextProps.limit
            });
        }

    }

    render() {
        const edit = (<Tooltip id="edit_tooltip">Edit Task</Tooltip>);
        const remove = (<Tooltip id="remove_tooltip">Remove</Tooltip>)
        return (
            <div style={{ display: "flex", flexDirection: "column" }}>
                <Table striped hover>
                    <thead>
                        <tr>
                            <th>Row No.</th>
                            <th>Ads</th>
                            <th>Operations</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.ads.map((a, key) => {
                                const edit = {
                                    pathname: `/create_ads/${a._id}`,
                                    ads: a
                                };
                                return (
                                    <tr>
                                        <td>{key + 1}</td>
                                        <td>{a.ads}</td>
                                        <td>
                                            <NavLink to={edit}><i className="fa fa-edit" /></NavLink>
                                            <OverlayTrigger placement="top" overlay={remove}>
                                                <Button
                                                    style={{ borderWidth: 0 }}
                                                    bsStyle="danger"
                                                    simple
                                                    type="button"
                                                    bsSize="xs"
                                                    onClick={(e) => {
                                                        deleteAds(a._id)
                                                    }}
                                                >
                                                    <i className="fa fa-times">
                                                    </i>
                                                </Button>
                                            </OverlayTrigger>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </Table>

                <Pagination bsSize="medium"
                    style={{ alignSelf: 'center' }}
                    ellipsis
                    boundaryLinks
                    items={Math.ceil(this.state.total / this.state.limit) === 1 ?
                        0 :
                        Math.ceil(this.state.total / this.state.limit)}
                    maxButtons={10}
                    activePage={this.state.active}
                    onSelect={this.loadPage} />
            </div>
        );
    }
}

AdsTable.propTypes = {};
AdsTable.defaultProps = {};

export default AdsTable;
