import React, { Component } from 'react';
import {Button, OverlayTrigger, Pagination, Table, Tooltip} from "react-bootstrap";
import { NavLink } from "react-router-dom";
import { getGamesByPage, deleteGame } from "../../operations/index";

class GamesTable extends Component {

    loadPage = (page) => {
        let skip = this.state.limit * (page - 1);
        getGamesByPage(skip).then(res => {
            this.setState({
                games: res.data,
                active: page
            })
        }).catch(err => alert(err));
    };

    constructor(props) {
        super(props);
        this.state = {
            total: 0,
            skip: 0,
            limit: 0,
            games: [],
            active: 1,
        }
    }

    static getStatus(game) {
        if (game.scheduled === "true")
            return "Scheduled";
        else if (game.started === "true")
            return "Started";
        else if (game.finished === "true")
            return "Finished";
    }

    componentWillReceiveProps(nextProps, nextState) {
        if (this.props.games !== nextProps.games) {
            this.setState({
                games: nextProps.games,
                total: nextProps.total,
                skip: nextProps.skip,
                limit: nextProps.limit,
                questions: nextProps.questions
            });
        }

    }

    render() {
        const edit = (<Tooltip id="edit_tooltip">Edit Task</Tooltip>);
        const remove = (<Tooltip id="remove_tooltip">Remove</Tooltip>)
        return (
            <div style={{ display: "flex", flexDirection: "column" }}>
                <Table striped hover>
                    <thead>
                        <tr>
                            <th>Row No.</th>
                            <th>Game</th>
                            <th>Start Time</th>
                            <th>Prize</th>
                            <th>Type</th>
                            <th>Status</th>
                            <th>Options</th>
                            <th>Operations</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.games.map((g, key) => {
                                const newTo = {
                                    pathname: `/single_game/${g._id}`,
                                    game: g
                                };
                                const edit = {
                                    pathname: `/edit_game/${g._id}`,
                                    game: g
                                };
                                const gameDate = new Date(g.start_time).toString();
                                const gameTime = gameDate.substr(0, gameDate.lastIndexOf("GMT"));
                                return (
                                    <tr>
                                        <td>{key + 1}</td>
                                        <td>{g.game == '' ? 'not setted' : g.game}</td>
                                        <td>{gameDate === 'Invalid Date' ? 'not setted' : gameTime}</td>
                                        <td>{g.prize == '' ? 'not setted' : g.prize}</td>
                                        <td>{g.type}</td>
                                        <td>{GamesTable.getStatus(g) === undefined ? 'not setted' : GamesTable.getStatus(g)}</td>
                                        {console.log(GamesTable.getStatus(g))}
                                        <td><NavLink to={newTo}><i className="pe-7s-more"> </i></NavLink></td>
                                        <td>
                                            <NavLink to={edit}><i className="fa fa-edit" /></NavLink>
                                            <OverlayTrigger placement="top" overlay={remove}>
                                                <Button
                                                    style={{ borderWidth: 0 }}
                                                    bsStyle="danger"
                                                    simple
                                                    type="button"
                                                    bsSize="xs"
                                                    onClick={(e) => {
                                                        deleteGame(g._id)
                                                    }}
                                                >
                                                    <i className="fa fa-times">
                                                    </i>
                                                </Button>
                                            </OverlayTrigger>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </Table>

                <Pagination bsSize="medium"
                    style={{ alignSelf: 'center' }}
                    ellipsis
                    boundaryLinks
                    items={Math.ceil(this.state.total / this.state.limit) === 1 ?
                        0 :
                        Math.ceil(this.state.total / this.state.limit)}
                    maxButtons={10}
                    activePage={this.state.active}
                    onSelect={this.loadPage} />
            </div>
        );
    }
}

GamesTable.propTypes = {};
GamesTable.defaultProps = {};

export default GamesTable;
