import React, { Component } from 'react';
import {
    Button,
    ControlLabel,
    FormControl,
    InputGroup,
    MenuItem,
    NavDropdown,
    OverlayTrigger,
    Pagination,
    Table,
    Tooltip
} from "react-bootstrap";
import {
    deleteQuestions,
    getCategories, getQuestions,
    getQuestionsByCategories,
    getQuestionsByPage,
    getQuestionsByTags,
    getQuestionsByStatus
} from "../../operations/index";
import Checkbox from 'elements/CustomCheckbox/CustomCheckbox';
import EditQuestionModal from "../../views/EditQuestionModal/EditQuestionModal";
import { getTags, searchQuestions } from "../../operations";
import Select from "react-select";
import '../../assets/css/custom.css';

class QuestionTable extends Component {

    loadPage = (page) => {
        let skip = this.state.limit * (page - 1);
        getQuestionsByPage(skip).then(res => {
            this.setState({
                questions: res.data,
                active: page
            })
        }).catch(err => alert(err));
    };

    onStateChanged = (isChecked, _id) => {
        if (!isChecked) {
            this.setState({
                selectedIDs: this.state.selectedIDs.concat(_id)
            })
        } else {
            this.setState({
                selectedIDs:
                    this.state.selectedIDs.filter((q) => {
                        return q.id !== _id
                    })
            })
        }
    };

    handleHide = () => {
        this.setState({ showModal: false });
    };

    composeTagsForSelect = async () => {
        let tags = await getTags();
        let tagsForSelect = tags.data.map(tag => {
            return {
                value: tag.id, label: tag.text
            }
        });
        this.setState({
            tags: tagsForSelect
        })
    };

    composeCategoriesForSelect = async () => {
        let categories = await getCategories();
        let categoriesForSelect = categories.data.map(category => {
            return {
                value: category.name, label: category.name
            }
        });
        this.setState({
            categories: categoriesForSelect
        })
    };

    onTagSelected = (selectedTags) => {
        let tags = selectedTags.map(tagForSelect => {
            return tagForSelect.value;
        });
        if (tags.length === 0) {
            getQuestions().then(response => {
                this.setState({
                    total: response.total,
                    skip: response.skip,
                    limit: response.limit,
                    questions: response.data
                })
            });
        } else {
            getQuestionsByTags(tags).then(response => {
                this.setState({
                    total: response.total,
                    skip: response.skip,
                    limit: response.limit,
                    questions: response.data
                })
            });
        }
    };

    onCategorySelected = (selectedCategory) => {
        /*let categories = selectedCategories.map(categoryForSelect => {
            return categoryForSelect.name;
        });*/
        getQuestionsByCategories([selectedCategory.value]).then(response => {
            this.setState({
                total: response.total,
                skip: response.skip,
                limit: response.limit,
                questions: response.data
            })
        });

    };

    onQuestionSelected = (selectedQuestionStatus) => {
        getQuestionsByStatus([selectedQuestionStatus.value]).then(response => {
            this.setState({
                total: response.total,
                skip: response.skip,
                limit: response.limit,
                questions: response.data
            })
        });
    };

    constructor(props) {
        super(props);
        this.state = {
            total: 0,
            skip: 0,
            limit: 0,
            questions: [],
            active: 1,
            selectedIDs: [],
            showModal: false,
            selectedQuestion: {},
            tags: [],
            categories: [],
            question_type: []
        };
        this.composeTagsForSelect();
        this.composeCategoriesForSelect();
    }

    /*static getDerivedStateFromProps(props, state) {
        alert(JSON.stringify(props))
            return {
                total: props.total,
                skip: props.skip,
                limit: props.limit,
                questions: props.questions
            }
    }*/
    componentWillReceiveProps(nextProps, nextState) {
        if (this.props.questions !== nextProps.questions)
            this.setState({
                total: nextProps.total,
                skip: nextProps.skip,
                limit: nextProps.limit,
                questions: nextProps.questions
            }, () => {
                this.props.selectedValues.map(item => (
                    item.map(item_value => {
                        this.props.updateTable(item_value.id)
                    })
                ));
            })
    }

    render() {
        const edit = (<Tooltip id="edit_tooltip">Edit Task</Tooltip>);
        const remove = (<Tooltip id="remove_tooltip">Remove</Tooltip>);
        const { type, onChecked } = this.props;

        return (
            <div style={{ display: "flex", flexDirection: "column" }}>
                {/*<Modal
                    show={this.state.showModal}
                    container={this}
                    onHide={this.handleHide}
                    aria-labelledby="contained-modal-title"
                >
                    <Modal.Header closeButton>
                        <Modal.Title id="contained-modal-title">
                            Selected Questions
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        Questions List
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={() => this.setState({showModal: false})}>Close</Button>
                    </Modal.Footer>
                </Modal>
*/}
                <EditQuestionModal
                    showModal={this.state.showModal}
                    onHide={this.handleHide}
                    question={this.state.selectedQuestion} />

                <InputGroup className="col-md-12" style={{ marginBottom: 20 }}>
                    <FormControl
                        id="search"
                        type="text"
                        placeholder="Search"
                        inputRef={(input) => this.inputChoiceA = input}
                        onKeyPress={event => {
                            if (event.key === "Enter") {
                                console.log(this.inputChoiceA.value);
                                searchQuestions(this.inputChoiceA.value).then(response => {
                                    console.log(response.data);
                                })
                            }
                        }}
                    />
                    <InputGroup.Addon
                        style={{ backgroundColor: "#F1F1F1" }}><i className="pe-7s-search" />
                    </InputGroup.Addon>
                </InputGroup>

                <div className="col-md-12">

                    <div className="col-md-4">
                        <ControlLabel>Filter By Tag</ControlLabel>
                        <Select
                            isMulti={true}
                            onChange={(selectedValue) => this.onTagSelected(selectedValue)}
                            isSearchable={true}
                            options={this.state.tags} />
                    </div>

                    <div className="col-md-4">
                        <ControlLabel>Filter By Category</ControlLabel>
                        <Select
                            isMulti={false}
                            onChange={(selectedValue) => this.onCategorySelected(selectedValue)}
                            isSearchable={true}
                            options={this.state.categories} />
                    </div>

                    <div className="col-md-4">
                        <ControlLabel>Filter By Question Status</ControlLabel>
                        <Select
                            isMulti={false}
                            onChange={(selectedValue) => this.onQuestionSelected(selectedValue)}
                            isSearchable={true}
                            options={[{ value: "not_asked", label: "Not Asked" }, { value: "asked", label: "Asked" }]} />
                    </div>

                </div>


                <Table striped hover>
                    <thead>
                        <tr>
                            {type ? <th>Select</th> : <th>Row No.</th>}
                            <th>Questions</th>
                            <th>Asked In</th>
                            <th>Difficulty</th>
                            <th>Choices</th>
                            <th>Operation</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.questions.map((c, key) => {
                                return (
                                    <tr key={c._id}>
                                        {type ?
                                            <Checkbox
                                                id={c._id}
                                                table
                                                isChecked={this.state.selectedIDs.indexOf(c._id) !== -1}
                                                label={c.name}
                                                onStateChange={(isChecked) => {
                                                    this.onStateChanged(isChecked, c._id);
                                                    onChecked(isChecked, c._id, c.question, c.difficulty)
                                                }}
                                            /> : <td key={key}>{key + 1}</td>}
                                        <td key={c.question}>{c.question}</td>

                                        <td key={c.correctAnswer}>{"Not Asked Yet"}</td>
                                        <td key={c.duration}>{c.difficulty}</td>
                                        <td>
                                            <NavDropdown
                                                style={{ listStyleType: "none" }}
                                                eventKey={2}
                                                title={
                                                    <i className="pe-7s-angle-down-circle" />} noCaret
                                                id="basic-nav-dropdown">
                                                {
                                                    c.choices.map((ch, i) => {
                                                        return (
                                                            <MenuItem
                                                                eventKey={2.1}>{c.choices[i].identifier + ". " + c.choices[i].option}</MenuItem>
                                                        )
                                                    })
                                                }

                                            </NavDropdown>
                                        </td>
                                        <td>
                                            <OverlayTrigger placement="top" overlay={edit}>
                                                <Button
                                                    style={{ borderWidth: 0 }}
                                                    bsStyle="info"
                                                    simple
                                                    type="button"
                                                    bsSize="xs"
                                                    onClick={() => this.setState({
                                                        showModal: true,
                                                        selectedQuestion: c
                                                    })}
                                                >
                                                    <i className="fa fa-edit" />
                                                </Button>
                                            </OverlayTrigger>

                                            <OverlayTrigger placement="top" overlay={remove}>
                                                <Button
                                                    style={{ borderWidth: 0 }}
                                                    bsStyle="danger"
                                                    simple
                                                    type="button"
                                                    bsSize="xs"
                                                    onClick={(e) => {
                                                        deleteQuestions(c._id)
                                                    }}
                                                >
                                                    <i className="fa fa-times">
                                                    </i>
                                                </Button>
                                            </OverlayTrigger>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </Table>

                {/*type &&
                    <Button bsStyle="link" style={{ alignSelf: "flex-end" }} onClick={() => this.setState({ showModal: true })}>Show
                Selected</Button> */}

                <Pagination bsSize="medium"
                    style={{ alignSelf: 'center' }}
                    ellipsis
                    boundaryLinks
                    items={Math.ceil(this.state.total / this.state.limit) === 1 ?
                        0 :
                        Math.ceil(this.state.total / this.state.limit)}
                    maxButtons={5}
                    activePage={this.state.active}
                    onSelect={this.loadPage}
                />
            </div>
        );
    }
}

QuestionTable.propTypes = {};
QuestionTable.defaultProps = {};

export default QuestionTable;
