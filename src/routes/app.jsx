import Dashboard from 'views/Dashboard/Dashboard';
import Questions from 'views/Questions/Questions';
import News from 'views/News/News';
import Notifications from 'views/Notifications/Notifications';
import QuestionsList from "../views/QuestionsList/QuestionsList";
import Games from "../views/Games/Games";
import GamesList from "../views/GamesList/GamesList";
import SingleGames from "../views/SingleGame/SingleGames";
import EditGames from "../views/EditGame/EditGames";
import Login from "../views/Login/Login";
import Users from "../views/Users/Users";
import Icons from "../views/Icons/Icons";
import CreateAdvertisement from "../views/ADS/CreateAdvertisement";
import AdsList from "../views/ADSList/ADSList";

const appRoutes = [
   /* {in_menu: true, path: "/login", name: "Login", icon: "pe-7s-graph", component: Login},*/
    {in_menu: true, path: "/dashboard", name: "Dashboard", icon: "pe-7s-graph", component: Dashboard},
    {in_menu: true, path: "/questions", name: "Questions", icon: "pe-7s-help1", component: Questions},
    {in_menu: false, path: "/see_questions", name: "Questions List", icon: "pe-7s-note2", component: QuestionsList},
    {in_menu: true, path: "/news", name: "News", icon: "pe-7s-news-paper", component: News},
    /*{in_menu: true, path: "/icons", name: "Icons", icon: "pe-7s-science", component: Icons},
    {in_menu: true, path: "/maps", name: "Maps", icon: "pe-7s-map-marker", component: Maps},*/
    {in_menu: true, path: "/notifications", name: "Notifications", icon: "pe-7s-bell", component: Notifications},
    {in_menu: true, path: "/games", name: "Games", icon: "pe-7s-play", component: GamesList},
    {in_menu: true, path: "/user", name: "Users", icon: "pe-7s-users", component: Users},
    {in_menu: false, path: "/games_list", name: "Games List", icon: "pe-7s-bell", component: GamesList},
    {in_menu: false, path: "/single_game/:_game_id", name: "Single Game", icon: "pe-7s-bell", component: SingleGames},
    {in_menu: false, path: "/edit_game/:_game_id?", name: "Edit Game", icon: "fa fa-edit", component: EditGames},
    {in_menu: false,  path:"/create_ads/:_ads_id?", name: "Create ADS", icon: "pe-7s-portfolio", component: CreateAdvertisement},
    {in_menu: true, path: "/ads_list", name: "Ads List", icon: "pe-7s-portfolio", component: AdsList},
    {redirect: true, path: "/", to: "/dashboard", name: "Dashboard"}
];

export default appRoutes;
