import io from 'socket.io-client';
import feathers from '@feathersjs/client/dist/feathers';
import socketio from '@feathersjs/client/socketio';
import authentication from '@feathersjs/client/authentication';
import {isAuthenticated} from "../operations/index";

//export const socket = io("http://192.168.43.238:3030");
//export const socket = io("http://192.168.0.32:3030");
export const socket = io("http://localhost:3030");
//export const socket = io();
//export const socket = io("http://178.62.39.146:3030");
//export const socket = io("http://104.248.160.95:3030");
//export const socket = io("http://104.248.160.95:3030");

const client = feathers();

client.configure(socketio(socket,  { timeout: 15000 }));

client.configure(authentication({
    storage: window.localStorage
}));

export const authenticate = (email, password) => {
    if (typeof email !== "undefined"){
        return client.authenticate({
            strategy: 'local',
            email,
            password
        })
    }else{
        return client.authenticate();
    }
};

export default client