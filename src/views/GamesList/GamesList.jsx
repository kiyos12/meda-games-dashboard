import React, { Component } from 'react';
import { Col, FormControl, Grid, InputGroup, Row } from 'react-bootstrap';

import Card from 'components/Card/Card.jsx';
import { tdArray, thArray } from 'variables/Variables.jsx';
import { getGames, searchQuestions } from "../../operations/index";
import { NavLink } from "react-router-dom";
import GamesTable from "../../elements/GamesTable/GameTable";

class GamesList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            games: [],
            total: 0,
            skip: 0,
            limit: 0
        }
    }

    componentDidMount() {
        getGames().then((response) => {
            console.log(response.data);
            this.setState({
                games: response.data,
                total: response.total,
                skip: response.skip,
                limit: response.limit
            })
        })
    }


    render() {

        return (
            <div className="content">
                <div style={{ marginLeft: 20 }}>
                    <NavLink to={"/edit_game"} className="nav-link">
                        <p>{"Create Game"}</p>
                    </NavLink>
                </div>
                <Grid fluid>
                    <Row>
                        <Col md={12}>
                            <Card
                                title="List of Games"
                                category="List of games created"
                                ctTableFullWidth ctTableResponsive
                                content={
                                    <div className="content">
                                        <InputGroup className="col-md-12" style={{ marginBottom: 20 }}>
                                            <FormControl
                                                id="choiceA"
                                                type="text"
                                                placeholder="Search"
                                                inputRef={(input) => this.inputChoiceA = input}
                                                onKeyPress={event => {
                                                    if (event.key === "Enter") {
                                                        console.log(this.inputChoiceA.value);
                                                        searchQuestions(this.inputChoiceA.value).then(response => {
                                                            console.log(response.data);
                                                        })
                                                    }
                                                }}
                                            />
                                            <InputGroup.Addon
                                                style={{ backgroundColor: "#F1F1F1" }}><i className="pe-7s-search" />
                                            </InputGroup.Addon>
                                        </InputGroup>
                                        <GamesTable
                                            games={this.state.games}
                                            total={this.state.total}
                                            skip={this.state.skip}
                                            limit={this.state.limit}
                                            type={false} />
                                    </div>
                                }
                            />
                        </Col>

                    </Row>
                </Grid>
            </div>
        );
    }
}

export default GamesList;
