import React, { Component } from 'react';
import Card from "../../components/Card/Card";
import Button from 'elements/CustomButton/CustomButton.jsx';
import {
    Col,
    ControlLabel,
    FormControl,
    FormGroup,
    Row
} from "react-bootstrap";
import { createAds, updateAds } from "../../operations/index";
import { NavLink } from "react-router-dom";
import NotificationSystem from "react-notification-system";
import { FilePond, File, registerPlugin } from 'react-filepond';
import 'filepond/dist/filepond.min.css';

// Import the Image EXIF Orientation and Image Preview plugins
// Note: These need to be installed separately
import FilePondPluginImageExifOrientation from 'filepond-plugin-image-exif-orientation';
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css';

// Register the fileupload plugins
registerPlugin(FilePondPluginImageExifOrientation, FilePondPluginImagePreview);

class CreateAdvertisement extends Component {

    addNotification = (message) => {
        this.refs.notificationSystem.addNotification({
            message: message,
            level: 'success',
            position: "tc"
        })
    };

    saveAds = () => {

        if (this.inputAds.value.length === 0) {
            this.setState({
                ads_name_validation: "error"
            });
            return
        }

        if (this.state.files.length === 0) {
            this.setState({
                files_validation: "error"
            });
            return
        }

        this.setState({ spin: "fa fa-spinner fa-spin" });

        if (this.state.ads_id) {
            // update the existing record
            updateAds(this.state.ads_id, {
                "ads": this.ads.value,
                "files": this.state.files
            }).then((response) => {
                this.addNotification(`"${response.game}" Advertisements updated successfully`);
                this.setState({ spin: "" });
            });
        }
        else {
            // create this object as new because there is no id found.
            createAds({
                "ads": this.ads.value,
                "files": this.state.files
            }).then((response) => {
                this.addNotification(`"${response.game}" Advertisements added successfully`);
                this.setState({ spin: "" });
            });
        }
    }

    handleInit() {
        console.log('FilePond instance has initialised', this.pond);
    }

    constructor(props) {
        super(props);
        this.state = {
            ads: "",
            files: [],
            //validations
            ads_name_validation: null,
            files_validation: null,
        }
    }

    render() {
        return (
            <div className="content">
                <div style={{ marginLeft: 20 }}>
                    <NavLink to={"/ads_list"} className="nav-link">
                        <p>{"See Advertisements/Ads"}</p>
                    </NavLink>
                </div>
                <Card
                    className="col-md-9"
                    title="Manage Advertisement/ADS"
                    content={
                        <form>
                            <FormGroup
                                validationState={this.state.ads_name_validation}>
                                <ControlLabel>Advertisement/ADS</ControlLabel>
                                <FormControl id="ads"
                                    type="text"
                                    required
                                    placeholder="Enter Advertisment..."
                                    value={this.state.ads}
                                    onChange={(e) => {
                                        this.setState({
                                            ads: e.target.value
                                        })
                                    }
                                    }
                                    inputRef={(input) => this.inputAds = input} />
                            </FormGroup>

                            <Row>
                                <Col>
                                    <FormGroup
                                        validationState={this.state.files_validation}>
                                        <FilePond ref={ref => this.pond = ref}
                                            allowMultiple={true}
                                            maxFiles={3}
                                            server="/api"
                                            oninit={() => this.handleInit()}
                                            onupdatefiles={(fileItems) => {
                                                // Set current file objects to this.state
                                                this.setState({
                                                    files: fileItems.map(fileItem => fileItem.file)
                                                });
                                            }}>

                                            {/* Update current files  */}
                                            {this.state.files.map(file => (
                                                <File key={file} src={file} origin="local" />
                                            ))}

                                        </FilePond>
                                    </FormGroup>
                                </Col>
                            </Row>

                            <Row></Row>

                            <Button
                                onClick={(e) => this.saveAds()}
                                bsStyle="info"
                                fill>
                                <i className={this.state.spin} />
                                Save Changes
                            </Button>
                            <NotificationSystem ref="notificationSystem" />

                        </form>
                    }
                />
            </div>
        );
    }
}

export default CreateAdvertisement;

