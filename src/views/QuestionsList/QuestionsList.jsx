import React, {Component} from 'react';
import {Col, FormControl, Grid, InputGroup, Row, Tooltip} from 'react-bootstrap';

import Card from 'components/Card/Card.jsx';
import {tdArray, thArray} from 'variables/Variables.jsx';
import {getQuestions, searchQuestions} from "../../operations/index";
import QuestionTable from "../../elements/QuestionTable/QuestionTable";

class QuestionsList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            questions: []
        }
    }

    componentDidMount() {
        getQuestions().then((response) => {
            this.setState({
                questions: response.data,
                total: response.total,
                skip: response.skip,
                limit: response.limit
            })
        })
    }


    render() {
        const edit = (<Tooltip id="edit_tooltip">Edit Task</Tooltip>);
        const remove = (<Tooltip id="remove_tooltip">Remove</Tooltip>);

        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col md={12}>
                            <Card
                                title="List of Questions"
                                category="Questions with the game they are asked"
                                ctTableFullWidth ctTableResponsive
                                content={
                                    <div className="content">
                                        <QuestionTable
                                            questions={this.state.questions}
                                            type={false}
                                            total={this.state.total}
                                            limit={this.state.limit}
                                            skip={this.state.skip}
                                        />
                                    </div>
                                }
                            />
                        </Col>

                    </Row>
                </Grid>
            </div>
        );
    }
}

export default QuestionsList;
