import React, { Component } from 'react';
import Card from "../../components/Card/Card";
import QuestionTable from "../../elements/QuestionTable/QuestionTable";
import { getQuestions } from "../../operations";
import Button from 'elements/CustomButton/CustomButton.jsx';
import { Col, ControlLabel, FormControl, FormGroup, Row, ToggleButtonGroup, ButtonToolbar, ToggleButton } from "react-bootstrap";
import { createGame } from "../../operations/index";
import { NavLink } from "react-router-dom";
import client from "../../api/index";
import NotificationSystem from "react-notification-system";

class Games extends Component {

    onStateChanged = (isChecked, _id, question, difficulty) => {
        if (!isChecked) {
            this.setState({
                questions_list: this.state.questions_list.concat({
                    "id": _id,
                    "question": question,
                    "difficulty": difficulty
                })
            })
        } else {
            this.setState({
                questions_list: this.state.questions_list.filter((question) => {
                    return question.id !== _id
                })
            })
        }
    };

    addNotification = (message) => {
        this.refs.notificationSystem.addNotification({
            message: message,
            level: 'success',
            position: "tc"
        })
    };

    createGameLocal = () => {

        if (this.inputGame.value.length === 0) {
            this.setState({
                game_name_validation: "error"
            });
            return
        }
        if(this.state.value === 0) {
            this.setState({
                game_type_validation: "error"
            });
            return
        }
        this.setState({ spin: "fa fa-spinner fa-spin" });
        const start_time = new Date(this.inputStartDate.value + "T" + this.inputStartTime.value).toUTCString();
        this.state.questions_list.map((q, i) => {
            q.row_no = i + 1
        });

        createGame({
            "game": this.inputGame.value,
            "start_date": this.inputStartDate.value,
            "start_time": start_time,
            "prize": this.inputPrize.value,
            "questions": this.state.questions_list,
            "scheduled": "false",
            "started": "false",
            "finished": "false",
            "type": this.state.value
        }).then((response) => {
            this.addNotification(`"${response.game}" Game added successfully`);
            this.setState({ spin: "" });
        })
    }
    questionCreated = (question) => {
        this.setState({
            questions: this.state.questions.concat(question)
        })
    }

    handleChange(e) {
        this.setState({ value: e });
        console.log(this.state.value);
    }

    constructor(props,context) {
        super(props, context);
        this.handleChange = this.handleChange.bind(this);
        this.state = {
            questions: [],
            questions_list: [],
            spin: "",
            value: 'unattended',
            //validations
            game_name_validation: null,
            game_prize_validation: null,
            game_date_validation: null,
            game_time_validation: null,
        }
    }

    componentWillMount() {
        getQuestions().then((response) => {
            this.setState({
                questions: response.data,
                total: response.total,
                skip: response.skip,
                limit: response.limit
            })
        })
    }

    componentDidMount() {
        client.service('question').on('created', this.questionCreated)
    }

    render() {

        return (
            <div className="content">
                <div style={{ marginLeft: 20 }}>
                    <NavLink to={"/games_list"} className="nav-link">
                        <p>{"See Games"}</p>
                    </NavLink>
                </div>
                <Card
                    className="col-md-9"
                    title="Add Game"
                    content={
                        <form>
                            <FormGroup
                                validationState={this.state.game_name_validation}>
                                <ControlLabel>Game</ControlLabel>
                                <FormControl id="game"
                                    type="text"
                                    required
                                    placeholder="Enter Game..."
                                    inputRef={(input) => this.inputGame = input} />
                            </FormGroup>

                            <FormGroup validationState={this.game_type_validation}>
                                <ControlLabel>Game Type</ControlLabel>
                                <ButtonToolbar>
                                    <ToggleButtonGroup
                                     type="radio"
                                     name="type"
                                     value={this.state.value}
                                     onChange={this.handleChange}
                                    >
                                        <ToggleButton active value={'unattended'}>Unattended</ToggleButton>
                                        <ToggleButton value={'attended'}>Attended</ToggleButton>
                                    </ToggleButtonGroup>
                                </ButtonToolbar>
                            </FormGroup>

                            <Row>
                                <Col>
                                    <FormGroup className="col-md-6" validationState={this.state.game_date_validation}>
                                        <ControlLabel>Start Date</ControlLabel>
                                        <FormControl id="start_date"
                                            type="date"
                                            placeholder="Enter Game Start Date..."
                                            inputRef={(input) => this.inputStartDate = input} />
                                    </FormGroup>

                                    <FormGroup className="col-md-6" validationState={this.state.game_time_validation}>
                                        <ControlLabel>Start Time</ControlLabel>
                                        <FormControl id="start_time"
                                            type="time"
                                            placeholder="Enter Game. Start Time.."
                                            inputRef={(input) => this.inputStartTime = input} />
                                    </FormGroup>
                                </Col>
                            </Row>

                            <FormGroup validationState={this.state.game_prize_validation}>
                                <ControlLabel>Prize</ControlLabel>
                                <FormControl id="prize"
                                    type="number"
                                    placeholder="Prize Amount..."
                                    inputRef={(input) => this.inputPrize = input} />
                            </FormGroup>
                            <Row>

                            </Row>

                            <Card
                                content={
                                    <div>
                                        <ControlLabel>Selected Questions
                                            ({this.state.questions_list.length})</ControlLabel>

                                        <QuestionTable
                                            checkedItems={this.state.questions_list}
                                            onChecked={(isChecked, _id, question, difficulty) => this.onStateChanged(isChecked, _id, question, difficulty)}
                                            questions={this.state.questions}
                                            type={true}
                                            total={this.state.total}
                                            limit={this.state.limit}
                                            skip={this.state.skip}
                                        />

                                    </div>
                                }
                            />

                            <Button
                                onClick={(e) => this.createGameLocal()}
                                bsStyle="info"
                                fill>
                                <i className={this.state.spin} />
                                Add Game
                            </Button>
                            <NotificationSystem ref="notificationSystem" />

                        </form>
                    }
                />
            </div>
        );
    }
}

Games.propTypes = {};
Games.defaultProps = {};

export default Games;

