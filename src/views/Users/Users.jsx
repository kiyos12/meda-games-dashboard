import React, {Component} from 'react';
import {
    Button,
    Col,
    FormControl,
    Grid,
    InputGroup,
    OverlayTrigger,
    Pagination,
    Row,
    Table,
    Tooltip
} from 'react-bootstrap';

import Card from 'components/Card/Card.jsx';
import {tdArray, thArray} from 'variables/Variables.jsx';
import {deleteQuestions, getUsers, getUsersByPage, searchQuestions} from "../../operations/index";

class Users extends Component {

    loadPage = (page) => {
        let skip = this.state.limit * (page - 1);
        getUsersByPage(skip).then(res => {
            this.setState({
                users: res.data,
                active: page
            })
        }).catch(err => alert(err));
    };

    constructor(props) {
        super(props);
        this.state = {
            users: [],
            total: 0,
            skip: 0,
            limit: 0,
            active: 1,
        }
    }

    componentDidMount() {
        getUsers().then((response) => {
            this.setState({
                users: response.data,
                limit: response.limit,
                skip: response.skip,
                total: response.total,
            })
        });
    }


    render() {
        const edit = (<Tooltip id="edit_tooltip">Edit Task</Tooltip>);
        const remove = (<Tooltip id="remove_tooltip">Remove</Tooltip>);

        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col md={12}>
                            <Card
                                title="Users List"
                                ctTableFullWidth ctTableResponsive
                                content={
                                    <div className="content">
                                        <InputGroup className="col-md-12" style={{marginBottom: 20}}>
                                            <FormControl
                                                id="choiceA"
                                                type="text"
                                                placeholder="Search"
                                                inputRef={(input) => this.inputChoiceA = input}
                                                onKeyPress={event => {
                                                    if (event.key === "Enter") {
                                                        console.log(this.inputChoiceA.value);
                                                        searchQuestions(this.inputChoiceA.value).then(response => {
                                                            console.log(response.data);
                                                        })
                                                    }
                                                }}
                                            />
                                            <InputGroup.Addon
                                                style={{backgroundColor: "#F1F1F1"}}><i className="pe-7s-search"/>
                                            </InputGroup.Addon>
                                        </InputGroup>
                                        <Table striped hover>
                                            <thead>
                                            <tr>
                                                <th>Row No.</th>
                                                <th>Full Name</th>
                                                <th>Phone Number</th>
                                                <th>Won</th>
                                                <th>High score</th>
                                                <th>Life</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {
                                                this.state.users.map((u, key) => {
                                                    return (
                                                        <tr key={key}>
                                                            <td key={key}>{key + 1}</td>
                                                            <td key={u.meda_name}>{u.meda_name}</td>

                                                            <td key={u.phone_number}>{"+" + u.phone_number}</td>
                                                            <td key={"won_" + u.won}>{u.won + ""}</td>
                                                            <td key={"highscore_" + u.highscore}>{u.highscore + ""}</td>
                                                            <td key={"life_" + u.life}>{u.life + ""}</td>
                                                            <td>
                                                                <OverlayTrigger placement="top" overlay={edit}>
                                                                    <Button
                                                                        style={{borderWidth: 0}}
                                                                        bsStyle="info"
                                                                        simple
                                                                        type="button"
                                                                        bsSize="xs"
                                                                    >
                                                                        <i className="fa fa-edit"></i>
                                                                    </Button>
                                                                </OverlayTrigger>

                                                                <OverlayTrigger placement="top" overlay={remove}>
                                                                    <Button
                                                                        style={{borderWidth: 0}}
                                                                        bsStyle="danger"
                                                                        simple
                                                                        type="button"
                                                                        bsSize="xs"
                                                                        onClick={(e) => {
                                                                            deleteQuestions(u._id)
                                                                        }}
                                                                    >
                                                                        <i className="fa fa-times"></i>
                                                                    </Button>
                                                                </OverlayTrigger>
                                                            </td>
                                                        </tr>
                                                    )
                                                })
                                            }
                                            </tbody>
                                        </Table>
                                    </div>
                                }
                            />
                        </Col>

                    </Row>
                </Grid>

                <div style={{width: "100%", display:'flex', flexDirection:'row', justifyContent:'center'}}>
                    <Pagination bsSize="medium"
                                style={{alignItems: 'center'}}
                                ellipsis
                                boundaryLinks
                                items={Math.ceil(this.state.total / this.state.limit) === 1 ?
                                    0 :
                                    Math.ceil(this.state.total / this.state.limit)}
                                maxButtons={10}
                                activePage={this.state.active}
                                onSelect={this.loadPage}/>
                </div>


            </div>
        );
    }
}

export default Users;
