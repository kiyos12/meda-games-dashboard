import React, { Component } from 'react';
import Card from "../../components/Card/Card";
import QuestionTable from "../../elements/QuestionTable/QuestionTable";
import { getQuestions } from "../../operations";
import Button from 'elements/CustomButton/CustomButton.jsx';
import {
    Col,
    ControlLabel,
    FormControl,
    FormGroup,
    Row,
    ToggleButtonGroup,
    ButtonToolbar,
    ToggleButton,
    Table
} from "react-bootstrap";
import { createGame, updateGame, getGame } from "../../operations/index";
import { NavLink } from "react-router-dom";
import client from "../../api/index";
import NotificationSystem from "react-notification-system";

class EditGames extends Component {

    updateQuestionTable = (_id) => {
        let index = this.state.questions.findIndex(question => question._id === _id);
        let unselected_qns = this.state.questions;
        if (index !== -1) {
            unselected_qns.splice(index, 1);
            this.setState({
                questions: unselected_qns
            });
        }
    }

    onStateChanged = (isChecked, _id, question, difficulty) => {
        if (!isChecked) {
            this.setState({
                questions_list: this.state.questions_list.concat({
                    "id": _id,
                    "question": question,
                    "difficulty": difficulty
                })
            })
            this.updateQuestionTable(_id);

        } else {
            this.setState({
                questions_list: this.state.questions_list.filter((question) => {
                    return question.id !== _id
                })
            })
            this.updateQuestionTable(_id);
        }
    };

    addNotification = (message) => {
        this.refs.notificationSystem.addNotification({
            message: message,
            level: 'success',
            position: "tc"
        })
    };

    saveGame = () => {

        if (this.inputGame.value.length === 0) {
            this.setState({
                game_name_validation: "error"
            });
            return
        }

        if (this.state.value === 0) {
            this.setState({
                game_type_validation: "error"
            });
            return
        }

        this.setState({ spin: "fa fa-spinner fa-spin" });
        const start_time = new Date(this.inputStartDate.value + "T" + this.inputStartTime.value).toUTCString();
        this.state.questions_list.map((q, i) => {
            q.row_no = i + 1
        });

        if (this.state.game_id) {
            // update the existing record
            updateGame(this.state.game_id, {
                "game": this.inputGame.value,
                "start_date": this.inputStartDate.value,
                "start_time": start_time,
                "prize": this.inputPrize.value,
                "questions": this.state.questions_list,
                "scheduled": "false",
                "started": "false",
                "finished": "false",
                "type": this.state.value
            }).then((response) => {
                this.addNotification(`"${response.game}" Game updated successfully`);
                this.setState({ spin: "" });
            });
        }
        else {
            // create this object as new because there is no id found.
            createGame({
                "game": this.inputGame.value,
                "start_date": this.inputStartDate.value,
                "start_time": start_time,
                "prize": this.inputPrize.value,
                "questions": this.state.questions_list,
                "scheduled": "false",
                "started": "false",
                "finished": "false",
                "type": this.state.value
            }).then((response) => {
                this.addNotification(`"${response.game}" Game added successfully`);
                this.setState({ spin: "" });
            });
        }
    }

    questionCreated = (question) => {
        this.setState({
            questions: this.state.questions.concat(question)
        })
    }

    handleChange(e) {
        this.setState({ value: e });
        console.log(this.state.value);
    }

    constructor(props, context) {
        super(props, context);
        this.state = {
            game: "",
            start_date: "",
            start_time: "",
            questions: [],
            selected_values: [],
            questions_list: [],
            prize: "",
            spin: "",
            value: 'unattended',
            //validations
            game_name_validation: null,
            game_prize_validation: null,
            game_date_validation: null,
            game_time_validation: null,
        }
        this.handleChange = this.handleChange.bind(this);
    }

    componentWillMount() {
        getQuestions().then((response) => {
            this.setState({
                questions: response.data,
                total: response.total,
                skip: response.skip,
                limit: response.limit
            })
        })
    }

    componentDidMount() {
        const { _game_id } = this.props.match.params;

        const gameObj = this.props.location.game;

        let selectedValues = [];
        let d;
        if (typeof gameObj === "undefined") {
            getGame(_game_id).then((response) => {
                this.setState({
                    game: response.game,
                    game_id: response._id,
                    value: response.type,
                    start_date: response.start_date,
                    prize: response.prize,
                    questions_list: response.questions
                })
                selectedValues.push(response.questions);
                d = response.start_time;
            })
        } else {
            this.setState({
                game: gameObj.game,
                game_id: gameObj._id,
                value: gameObj.type,
                start_date: gameObj.start_date,
                prize: gameObj.prize,
                questions_list: gameObj.questions
            })
            selectedValues.push(gameObj.questions);
            d = gameObj.start_time;
        }
        this.setState({selected_values: selectedValues});
        
        console.log(d);
        let h = new Date(d).getHours();
        let m = new Date(d).getMinutes();
        if (h < 10) h = '0' + h;
        if (m < 10) m = '0' + m;
        this.setState({
            start_time: `${h}:${m}`
        });

        client.service('question').on('created', this.questionCreated)
    }

    render() {
        return (
            <div className="content">
                <div style={{ marginLeft: 20 }}>
                    <NavLink to={"/games_list"} className="nav-link">
                        <p>{"See Games"}</p>
                    </NavLink>
                </div>
                <Card
                    className="col-md-9"
                    title="Manage Game"
                    content={
                        <form>
                            <FormGroup
                                validationState={this.state.game_name_validation}>
                                <ControlLabel>Game</ControlLabel>
                                <FormControl id="game"
                                    type="text"
                                    required
                                    placeholder="Enter Game..."
                                    value={this.state.game}
                                    onChange={(e) => {
                                        this.setState({
                                            game: e.target.value
                                        })
                                    }
                                    }
                                    inputRef={(input) => this.inputGame = input} />
                            </FormGroup>

                            <FormGroup validationState={this.game_type_validation}>
                                <ControlLabel>Game Type</ControlLabel>
                                <ButtonToolbar>
                                    <ToggleButtonGroup
                                        type="radio"
                                        name="type"
                                        value={this.state.value}
                                        onChange={this.handleChange}
                                    >
                                        <ToggleButton value={'unattended'}>Unattended</ToggleButton>
                                        <ToggleButton value={'attended'}>Attended</ToggleButton>
                                    </ToggleButtonGroup>
                                </ButtonToolbar>
                            </FormGroup>

                            <Row>
                                <Col>
                                    <FormGroup className="col-md-6" validationState={this.state.game_date_validation}>
                                        <ControlLabel>Start Date</ControlLabel>
                                        <FormControl id="start_date"
                                            type="date"
                                            placeholder="Enter Game Start Date..."
                                            value={this.state.start_date}
                                            onChange={(e) => {
                                                this.setState({
                                                    start_date: e.target.value
                                                })
                                            }
                                            }
                                            inputRef={(input) => this.inputStartDate = input} />
                                    </FormGroup>

                                    <FormGroup className="col-md-6" validationState={this.state.game_time_validation}>
                                        <ControlLabel>Start Time</ControlLabel>
                                        <FormControl id="start_time"
                                            type="time"
                                            placeholder="Enter Game. Start Time.."
                                            value={this.state.start_time}
                                            onChange={(e) => {
                                                this.setState({
                                                    start_time: e.target.value
                                                })
                                            }
                                            }
                                            inputRef={(input) => this.inputStartTime = input} />
                                    </FormGroup>
                                </Col>
                            </Row>

                            <FormGroup validationState={this.state.game_prize_validation}>
                                <ControlLabel>Prize</ControlLabel>
                                <FormControl id="prize"
                                    type="number"
                                    placeholder="Prize Amount..."
                                    value={this.state.prize}
                                    onChange={(e) => {
                                        this.setState({
                                            prize: e.target.value
                                        })
                                    }
                                    }
                                    inputRef={(input) => this.inputPrize = input} />
                            </FormGroup>
                            <Row>
                            </Row>

                            <Card
                                content={
                                    <div>
                                        <ControlLabel>Question Bank</ControlLabel>

                                        <QuestionTable
                                            checkedItems={this.state.questions_list}
                                            onChecked={(isChecked, _id, question, difficulty) => this.onStateChanged(isChecked, _id, question, difficulty)}
                                            questions={this.state.questions}
                                            type={true}
                                            total={this.state.total}
                                            limit={this.state.limit}
                                            skip={this.state.skip}
                                            selectedQns={this.state.selected_values}
                                            updateTable={this.updateQuestionTable}
                                            selectedValues={this.state.selected_values}
                                        />
                                    </div>
                                }
                            />
                            <Card
                                content={
                                    <div>
                                        <ControlLabel>
                                            Selected Questions
                                          ({this.state.questions_list.length})
                                      </ControlLabel>
                                        <Table striped hover>
                                            <thead>
                                                <tr>
                                                    <th>Row No.</th>
                                                    <th>Questions</th>
                                                    <th>Asked In</th>
                                                    <th>Difficulty</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    this.state.questions_list.map((c, key) => {
                                                        return (
                                                            <tr key={c._id}>
                                                                <td key={key}>{key + 1}</td>
                                                                <td key={c.question}>{c.question}</td>
                                                                <td key={c.correctAnswer}>{"Not Asked Yet"}</td>
                                                                <td key={c.duration}>{c.difficulty}</td>
                                                            </tr>
                                                        )
                                                    })
                                                }
                                            </tbody>
                                        </Table>
                                    </div>

                                }
                            />

                            <Button
                                onClick={(e) => this.saveGame()}
                                bsStyle="info"
                                fill>
                                <i className={this.state.spin} />
                                Save Changes
                            </Button>
                            <NotificationSystem ref="notificationSystem" />

                        </form>
                    }
                />
            </div>
        );
    }
}

EditGames.propTypes = {};
EditGames.defaultProps = {};

export default EditGames;

