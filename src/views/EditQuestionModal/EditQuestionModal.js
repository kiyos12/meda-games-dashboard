import React, {Component} from 'react';
import {Col, ControlLabel, FieldGroup, FormControl, FormGroup, Grid, InputGroup, Modal, Row} from 'react-bootstrap';
import {FormInputs} from 'components/FormInputs/FormInputs.jsx';
import {UserCard} from 'components/UserCard/UserCard.jsx';
import Button from 'elements/CustomButton/CustomButton.jsx';
import {updateQuestion} from "../../operations";
import NotificationSystem from "react-notification-system";
import Card from "../../components/Card/Card";
import Slider, {createSliderWithTooltip} from "rc-slider";
import 'rc-slider/assets/index.css';

const DifficultySlider = createSliderWithTooltip(Slider);

class EditQuestionModal extends Component {

    updateQuestionLocal = () => {
        this.setState({spin: "fa fa-spinner fa-spin"});

        updateQuestion(this.props.question._id, {
            "question": this.inputQuestion.value,
            "choices": [
                {"identifier": "A", "option": this.inputChoiceA.value},
                {"identifier": "B", "option": this.inputChoiceB.value},
                {"identifier": "C", "option": this.inputChoiceC.value},
            ],
            "correctAnswer": this.inputCorrectAnswer.value,
            "difficulty": this.state.difficulty,
            "explanation": this.inputExplanation.value,
            "asked_games": [],
        }).then((response) => {
            this.setState({spin: ""});
            this.addNotification(`Question "${response.question}" successfully updated`)
        });
    };

    addNotification = (message) => {
        this._notificationSystem.addNotification({
            message: message,
            level: 'success',
            position: "tc"
        })
    };

    state = {
        question: this.props.question.question,
        choiceA: typeof this.props.question.choices !== 'undefined' && this.props.question.choices[0].option,
        choiceB: typeof this.props.question.choices !== 'undefined' && this.props.question.choices[1].option,
        choiceC: typeof this.props.question.choices !== 'undefined' && this.props.question.choices[2].option,
        correctAnswer: "",
        duration: this.props.question.duration,
        spin: "",
        categories: [],
        toggleAddCategory: false,
        checkedCategories: [],
        difficulty: this.props.question.difficulty
    };

    componentDidMount() {
        const {question} = this.props;


        this._notificationSystem = this.refs.notificationSystem;
    };


    render() {
        const {question, showModal, hideModal} = this.props;

        return (
            <div className="content">
                <Modal
                    show={showModal}
                    container={this}
                    aria-labelledby="contained-modal-title"
                    onHide={hideModal}>

                    <Modal.Header closeButton>
                        <Modal.Title id="contained-modal-title">
                            Edit Question
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Grid fluid>
                            <Row>
                                <Card
                                    content={
                                        <form>
                                            <FormGroup>
                                                <ControlLabel>Questions</ControlLabel>
                                                <FormControl id="questions"
                                                             type="text"
                                                             placeholder="Enter Question..."
                                                             value={this.state.question}
                                                             onChange={(e)=> {
                                                                 this.setState({
                                                                     question: e.target.value
                                                                 })}
                                                             }
                                                             inputRef={(input) => this.inputQuestion = input}/>
                                            </FormGroup>

                                            <Card
                                                content={
                                                    <Col>
                                                        <ControlLabel>Choices</ControlLabel>
                                                        <InputGroup className="col-md-12">
                                                            <InputGroup.Addon
                                                                style={{backgroundColor: "#F1F1F1"}}>A. </InputGroup.Addon>
                                                            <FormControl
                                                                id="choiceA"
                                                                type="text"
                                                                placeholder="Choice A"
                                                                value={typeof question.choices !== 'undefined' && question.choices[0].option}
                                                                inputRef={(input) => this.inputChoiceA = input}
                                                            />
                                                        </InputGroup>

                                                        <InputGroup className="col-md-12">
                                                            <InputGroup.Addon
                                                                style={{backgroundColor: "#F1F1F1"}}>B. </InputGroup.Addon>
                                                            <FormControl
                                                                id="choiceB"
                                                                type="text"
                                                                placeholder="Choice B"
                                                                value={typeof question.choices !== 'undefined' && question.choices[1].option}
                                                                inputRef={(input) => this.inputChoiceB = input}
                                                            />
                                                        </InputGroup>

                                                        <InputGroup className="col-md-12">
                                                            <InputGroup.Addon
                                                                style={{backgroundColor: "#F1F1F1"}}>C. </InputGroup.Addon>
                                                            <FormControl
                                                                id="choiceC"
                                                                type="text"
                                                                placeholder="Choice C"
                                                                value={typeof question.choices !== 'undefined' && question.choices[2].option}
                                                                inputRef={(input) => this.inputChoiceC = input}
                                                            />
                                                        </InputGroup>

                                                        <div/>
                                                    </Col>
                                                }
                                            />

                                            <Card
                                                content={
                                                    <Col>

                                                        <FormGroup>
                                                            <ControlLabel>Correct Answer</ControlLabel>
                                                            <FormControl type="password"
                                                                         bsClass="form-control"
                                                                         placeholder="Letter"
                                                                         inputRef={(input) => this.inputCorrectAnswer = input}/>
                                                        </FormGroup>

                                                        <FormGroup>
                                                            <ControlLabel>Explanation</ControlLabel>
                                                            <FormControl type="text"
                                                                         bsClass="form-control"
                                                                         placeholder="Explanation"
                                                                         value={question.explanation}
                                                                         inputRef={(input) => this.inputExplanation = input}/>
                                                        </FormGroup>

                                                        <FormGroup>
                                                            <ControlLabel>Difficulty Slider</ControlLabel>
                                                            {/*<FormControl type="number"
                                                                     bsClass="form-control"
                                                                     placeholder="Duration in sec"
                                                                     inputRef={(input) => this.inputDuration = input}/>*/}
                                                            <DifficultySlider
                                                                style={{
                                                                    width: "99%",
                                                                    marginTop: 10,
                                                                    marginLeft: "1%",
                                                                }}
                                                                min={1}
                                                                max={10}
                                                                defaultValue={question.difficulty}
                                                                vertical={false}
                                                                marks={{
                                                                        1: "1",
                                                                        2: "2",
                                                                        3: "3",
                                                                        4: "4",
                                                                        5: "5",
                                                                        6: "6",
                                                                        7: "7",
                                                                        8: "8",
                                                                        9: "9",
                                                                        10: "10",
                                                                    }}
                                                                onChange={(value) => {
                                                                    this.setState({
                                                                        difficulty: value
                                                                    })
                                                                }}
                                                            />
                                                        </FormGroup>
                                                    </Col>

                                                }
                                            />

                                            <Button
                                                onClick={(e) => this.updateQuestionLocal()}
                                                bsStyle="info"
                                                pullRight
                                                fill>
                                                <i className={this.state.spin}/>
                                                Save Edit
                                            </Button>
                                            <div className="clearfix"/>
                                        </form>
                                    }
                                />
                            </Row>
                        </Grid>
                    </Modal.Body>
                </Modal>

                <NotificationSystem ref="notificationSystem"/>
            </div>
        );
    }
}

export default EditQuestionModal;