import React, { Component } from 'react';
import { Card } from "../../components/Card/Card";
import { Button, Col, ProgressBar } from "react-bootstrap";
import { getQuestion } from "../../operations/index";
import client from "../../api/index";
import { Jello } from 'react-motions'

class SingleQuestion extends Component {

    send = (id) => {
        if (this.state.btn_text == 'Send Question') {
            client.service('question').update(id, { $set: { send: "true" } })
                .then(response => console.log(response))
        }
        else {
            client.service('question').update(id, { $set: { send: "true" } })
                .then(response => console.log(response))
        }
    };

    constructor(props) {
        super(props);
        this.state = {
            question: {},
            isActive: false,
            progress: 0,
            btn_text: 'Send Question',
            toggle_send: 'question',
            interval: 0
        }
    }

    componentDidMount() {
        getQuestion(this.props.questionId).then(response => {
            console.log("Response", response, this.props.questionId);
            this.setState({
                question: response.data[0]
            })
        });

        client.service('question').on('updated', question => {
            if (question._id === this.props.questionId.id) {
                let server_time = question.server_time;
                let local_time = new Date().getTime();
                console.log(server_time, local_time);
                let count_down_to = 0;
                this.interval = setInterval(() => {
                    console.log(count_down_to);
                    this.setState({
                        progress: count_down_to
                    });
                    count_down_to += 10;
                    if (this.state.progress == 100) {
                        clearInterval(this.interval);
                        if (this.state.btn_text == 'Send Question') {
                            this.setState({
                                btn_text: 'Send Answer',
                                progress: 0
                            })
                        }
                        else {
                            this.setState({
                                btn_text: 'Send Question',
                                progress: 0
                            })
                        }
                    }
                }, 1000);
                console.log(this.interval);

            } else {
                this.setState({ progress: 0 })
            }
        });
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    render() {
        const { question, id, difficulty } = this.props.questionId;

        return (
            <div>
                <Col style={{ width:'35%', cursor: 'move' }}>
                    <Card
                        content={
                            <div>
                                <p>{question}</p>
                                <hr />
                                <div style={styles.bottomWrapper}>
                                    {this.state.isActive &&
                                        <Jello duration={1} infinite>
                                            <div style={styles.activeDot} />
                                        </Jello>}
                                    <h5>Difficulty : {difficulty}</h5>

                                    <Button bsStyle="link" style={{ borderWidth: 0, alignSelf: 'center' }}
                                        onClick={(e) => this.send(id)}>
                                        {this.state.btn_text}
                                    </Button>
                                </div>
                                {this.state.progress !== 0 && <ProgressBar now={this.state.progress} />}

                            </div>
                        }
                    />
                </Col>
            </div>
        );
    }
}

const styles = {
    bottomWrapper: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    activeDot: {
        height: "20px",
        width: "20px",
        backgroundColor: "#0f0",
        borderRadius: "50%",
    },
}

SingleQuestion.propTypes = {};
SingleQuestion.defaultProps = {};

export default SingleQuestion;
