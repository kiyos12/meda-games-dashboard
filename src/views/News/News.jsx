import React, {Component} from 'react';
import {Col, ControlLabel, FormControl, FormGroup, Grid, Row} from 'react-bootstrap';
import Button from '../../elements/CustomButton/CustomButton';
import Card from 'components/Card/Card.jsx'
import {createNews, getGames} from "../../operations/index";

class News extends Component {

    addNewsField = () => {
        this.setState({
            numChildren: ++this.state.numChildren
        })
    };

    createNewsLocal = () => {
        let news = [];
        for (let i = 0; i < this.state.numChildren; i++) {
            if (this.state[`input${i + 1}`] !== null)
                news.push(this.state[`input${i + 1}`])
        }
        createNews({
            game: this.inputGame.value,
            announcement: this.inputAnnouncement.value,
            news
        }).then((res) => {
            alert("Game News Successfully created");
        })
    };

    inputChangeHandler = (event) => {
        let key = event.target.id;
        let val = event.target.value;
        let obj = {};
        obj[key] = val;
        this.setState(obj)
    };

    constructor(props) {
        super(props);
        this.state = {games: [], numChildren: 0}
    }

    componentDidMount() {
        getGames().then(res => {
            this.setState({
                games: res.data
            })
        })
    }

    render() {
        const children = [];

        for (let i = 0; i < this.state.numChildren; i++) {

            children.push(
                <FormControl
                    key={`input${i + 1}`}
                    style={{marginTop: 15, marginBottom: 15}}
                    id={`input${i + 1}`}
                    type="text"
                    autoFocus={(children.length > 2) && true}
                    onChange={event => this.inputChangeHandler(event)}
                    placeholder={`News (Notice) ${i + 1}`}
                />
            );
        }

        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col md={12}>
                            <Card
                                title="Add News"
                                category="Content to be shown before the game begins"
                                content={
                                    <form>
                                        <FormGroup>
                                            <ControlLabel>Assign Game</ControlLabel>
                                            <FormControl componentClass="select"
                                                         inputRef={input => this.inputGame = input}>
                                                <option value="select" disabled={true} selected={true}>Select Game
                                                </option>
                                                {
                                                    this.state.games.map((g, i) => {
                                                        return (
                                                            <option value={g._id}>{g.start_time}</option>
                                                        )
                                                    })
                                                }
                                            </FormControl>
                                        </FormGroup>

                                        <Card
                                            content={
                                                <div>
                                                    <ControlLabel>Pre-game Announcement</ControlLabel>
                                                    <FormControl
                                                        style={{marginTop: 15, marginBottom: 15}}
                                                        inputRef={input => this.inputAnnouncement = input}
                                                        type="text"
                                                        autoFocus={(children.length > 2) && true}
                                                        onChange={event => this.inputChangeHandler(event)}
                                                        placeholder={"Announcement"}
                                                    />
                                                </div>
                                            }
                                        />

                                        <Card
                                            content={
                                                <div>
                                                    <ControlLabel>News (Notice)</ControlLabel>
                                                    {children}
                                                    <Button
                                                        onClick={(e) => this.addNewsField()}
                                                        bsStyle="link" style={{borderWidth: 0}}>Add News</Button>
                                                </div>
                                            }
                                        />

                                        <Button
                                            onClick={(e) => this.createNewsLocal()}
                                            bsStyle="info"
                                            pullRight
                                            fill>
                                            Assign
                                        </Button>
                                        <div className="clearfix"/>
                                    </form>
                                }
                            />
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}

export default News;
