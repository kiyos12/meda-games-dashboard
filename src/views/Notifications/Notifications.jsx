import React, {Component} from 'react';
import {Col, ControlLabel, FormControl, FormGroup, Grid, Row} from 'react-bootstrap';
import Button from '../../elements/CustomButton/CustomButton';
import Card from 'components/Card/Card.jsx'
import {sendNotification} from "../../operations/index";


class Notifications extends Component {

    TYPE_ACTIVITY = "activity";
    TYPE_LINK = "link";
    TYPE_FRAGMENT = "fragment";
    TYPE_DISCOVER_CONTENT = "discover_content";
    TYPE_CHAT_SCREEN = "chat_screen";

    sendNotificationLocal = () => {
        sendNotification({
            "to": "/topics/meda",
            "data": {
                "title": this.inputNotificationTitle.value,
                "body": this.inputNotificationBody.value,
                "sound": "default",
                "click_action": this.generateClickAction()/*"{\"type\":\"link\", \"action\":\"https://www.facebook.com\"}"*/,
            },
            "ttl": 3600
        }).then(res => alert("Successfully sent")).catch(err => alert("Error " + err))
    };

    generateClickAction = () => {
        return {
            type: this.state.selected,
            action: this.inputNotificationAction.value,
        }
    };

    onActionTypeSelected = (e) => {
        this.setState({selected: this.inputNotificationActionType.value});
    };

    renderSelectedContent = () => {
        if (this.state.selected === this.TYPE_ACTIVITY) {
            return (
                <div>
                    <FormGroup>
                        <ControlLabel>Select Activity</ControlLabel>
                        <FormControl componentClass="select"
                                     inputRef={input => this.inputNotificationAction = input}
                                     onChange={(e) => this.onActivitySelected(e)}>
                            <option value="select" disabled={true} selected={true}>Select Activity
                            </option>
                            <option value="RootActivity">Root Activity</option>
                            <option value="ShiActivity">Shi Activity</option>
                            <option value="ProfileActivity">Profile Activity</option>
                            <option value="ChatActivity">Chat Activity</option>
                        </FormControl>
                    </FormGroup>

                    <FormGroup>
                        <ControlLabel>Extra Data</ControlLabel>
                        <FormControl
                            type="text"
                            inputRef={inp => this.inputNotificationExtraData = inp}
                            placeholder="Add Extra data like phone number for chat activity"
                        />
                    </FormGroup>
                </div>
            )
        }
        else if (this.state.selected === this.TYPE_LINK) {
            return (
                <FormGroup>
                    <ControlLabel>Select Activity</ControlLabel>
                    <FormControl
                        type="url"
                        autoFocus={true}
                        inputRef={inp => this.inputNotificationAction = inp}
                        placeholder="Insert Link Address"
                    />
                </FormGroup>
            )
        }
        else if (this.state.selected === this.TYPE_FRAGMENT) {
            return (
                <FormGroup>
                    <ControlLabel>Select Fragment</ControlLabel>
                    <FormControl componentClass="select"
                                 inputRef={input => this.inputNotificationAction = input}
                                 onChange={(e) => this.onActivitySelected(e)}>
                        <option value="select" disabled={true} selected={true}>Select Fragment
                        </option>
                        <option value="fr_root">Chat Fragment</option>
                        <option value="fr_compose">Contacts Fragment</option>
                        <option value="fr_game">Shi Fragment</option>
                        <option value="fr_discover">Discover Fragment</option>
                        <option value="fr_settings">Settings Fragment</option>
                    </FormControl>
                </FormGroup>
            )
        }
        else if (this.state.selected === this.TYPE_DISCOVER_CONTENT) {
            return (
                <FormGroup>
                    <ControlLabel>Add Discover Url</ControlLabel>
                    <FormControl
                        type="url"
                        autoFocus={true}
                        inputRef={inp => this.inputNotificationAction = inp}
                        placeholder="Insert Link Address"
                    />
                </FormGroup>
            )
        }
        else if (this.state.selected === this.TYPE_CHAT_SCREEN) {
            return (
                <FormGroup>
                    <ControlLabel>Add Phone Number</ControlLabel>
                    <FormControl
                        type="phone"
                        autoFocus={true}
                        inputRef={inp => this.inputNotificationAction = inp}
                        placeholder="Add desired phone number"
                    />
                </FormGroup>
            )
        }

    };

    onActivitySelected = (e) => {
        return undefined;
    };

    constructor(props) {
        super(props);
        this.state = {
            selected: ""
        }
    }

    render() {
        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col md={12}>
                            <Card
                                title="Send Notification"
                                category="Notify users for specific events"
                                content={
                                    <form>
                                        <FormGroup>
                                            <ControlLabel>Notification Title</ControlLabel>
                                            <FormControl
                                                type="text"
                                                autoFocus={true}
                                                inputRef={inp => this.inputNotificationTitle = inp}
                                                placeholder="Add Title"
                                            />
                                        </FormGroup>

                                        <FormGroup controlId="formControlsTextarea">
                                            <ControlLabel>Notification Text</ControlLabel>
                                            <FormControl componentClass="textarea"
                                                         placeholder="Add Notification Body (Not more than 160 chars)"
                                                         inputRef={inp => this.inputNotificationBody = inp}/>
                                        </FormGroup>

                                        <FormGroup>
                                            <ControlLabel>Select Click Action Type</ControlLabel>
                                            <FormControl componentClass="select"
                                                         inputRef={input => this.inputNotificationActionType = input}
                                                         onChange={(e) => this.onActionTypeSelected(e)}>
                                                <option value="select" disabled={true} selected={true}>Select Action Type
                                                </option>
                                                <option value="activity">Activity</option>
                                                <option value="link">Link</option>
                                                <option value="fragment">Fragment</option>
                                                <option value="discover_content">Discover Content</option>
                                                <option value="chat_screen">Chat Screen</option>
                                            </FormControl>
                                        </FormGroup>

                                        {this.renderSelectedContent()}

                                        <Button
                                            onClick={(e) => this.sendNotificationLocal()}
                                            bsStyle="info"
                                            pullRight
                                            fill>
                                            Send
                                        </Button>
                                        <div className="clearfix"/>
                                    </form>
                                }
                            />
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}

export default Notifications;
