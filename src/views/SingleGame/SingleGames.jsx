import React, {Component} from 'react';
import {Col, Grid} from 'react-bootstrap';
import Card from 'components/Card/Card.jsx';
import Button from 'elements/CustomButton/CustomButton.jsx';
import {tdArray, thArray} from 'variables/Variables.jsx';
import {deleteGame, getGame, reOrderQuestions, scheduleGame, startGames} from "../../operations/index";
import {arrayMove, SortableContainer, SortableElement} from "react-sortable-hoc";
import SingleQuestion from "../SingleQuestion/index";
import client from "../../api";

const SortableItem = SortableElement(({value}) =>
    renderQuestions(value)
);

client.service('game').on('usersAnswer', response => {
    //alert("usersAnswer " + JSON.stringify(response))
});

const SortableList = SortableContainer(({items}) => {
    return (
        <div>
            {items.map((value, index) => (
                <SortableItem
                    key={`item-${index}`}
                    index={index}
                    value={value}/>
            ))}
        </div>
    );
});

const renderQuestions = (q) => {
    return (
        <div>
            <SingleQuestion questionId={q}/>
        </div>
    )
};

class SingleGames extends Component {

    onSortEnd = ({oldIndex, newIndex}) => {
        this.setState({
            gQuestions: arrayMove(this.state.gQuestions, oldIndex, newIndex)
        });
        this.state.gQuestions.map((q, i) => {
            q.row_no = i + 1
        });
        reOrderQuestions(this.state.gQuestions, this.props.match.params._game_id)
    };

    constructor(props) {
        super(props);
        this.state = {
            game: {},
            gQuestions: [],
            questionsCounter: 0
        }
    }

    componentDidMount() {

        const {_game_id} = this.props.match.params;

        const gameObj = this.props.location.game;

        if (typeof gameObj === "undefined") {
            getGame(_game_id).then(response => {
                this.setState({
                    game: response,
                    gQuestions: response.questions
                })
            })
        } else {
            this.setState({
                game: gameObj,
                gQuestions: gameObj.questions
            })
        }
    }


    render() {
        const {_game_id} = this.props.match.params;
        const {scheduled} = this.state.game;
        return (
            <div className="content">
                <Col md={12}>
                    <Col md={8}>
                        <Card
                            category="Game detail"
                            content={
                                <div className="content">
                                    <Grid>
                                        <SortableList
                                            items={this.state.gQuestions}
                                            onSortEnd={this.onSortEnd}
                                            axis="y"/>
                                    </Grid>

                                    <div>
                                        <Button
                                            style={{marginRight: 15, marginLeft: 10}}
                                            onClick={(e) => {
                                                scheduleGame(_game_id).then((response) => {
                                                    alert("Done scheduling " + response);
                                                })
                                            }}
                                            bsStyle="info"
                                            fill>
                                            <i className={this.state.spin}/>
                                            {(scheduled === "true") ? "Game Scheduled" : "Schedule Game"}

                                        </Button>

                                        <Button
                                            onClick={(e) => {
                                                deleteGame(_game_id).then((response) => {
                                                    this.props.history.goBack();
                                                })
                                            }}
                                            bsStyle="danger"
                                            fill>
                                            Delete Game
                                        </Button>
                                    </div>


                                </div>
                            }
                        />
                    </Col>
                    <Col md={4}>
                        <Card
                            category="Game Data"
                            content={
                                <div/>
                            }
                        />
                    </Col>

                </Col>
            </div>
        );
    }
}

export default SingleGames;