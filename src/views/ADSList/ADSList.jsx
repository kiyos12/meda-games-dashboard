import React, {Component} from 'react';
import {Col, Grid, Row, Tooltip} from 'react-bootstrap';

import Card from 'components/Card/Card.jsx';
import {getAdverts} from "../../operations/index";
import AdsTable from "../../elements/AdsTable/AdsTable";
import { NavLink } from "react-router-dom";

class AdsList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            ads: []
        }
    }

    componentDidMount() {
        getAdverts().then((response) => {
            this.setState({
                ads: response.data,
                total: response.total,
                skip: response.skip,
                limit: response.limit
            })
        })
    }


    render() {
        const edit = (<Tooltip id="edit_tooltip">Edit Task</Tooltip>);
        const remove = (<Tooltip id="remove_tooltip">Remove</Tooltip>);

        return (
            <div className="content">
             <div style={{ marginLeft: 20 }}>
                    <NavLink to={"/create_ads"} className="nav-link">
                        <p>{"Create Advertisement/Ads"}</p>
                    </NavLink>
                </div>
                <Grid fluid>
                    <Row>
                        <Col md={12}>
                            <Card
                                title="List of Advertisements"
                                ctTableFullWidth ctTableResponsive
                                content={
                                    <div className="content">
                                        <AdsTable
                                            ads={this.state.ads}
                                            type={false}
                                            total={this.state.total}
                                            limit={this.state.limit}
                                            skip={this.state.skip}
                                        />
                                    </div>
                                }
                            />
                        </Col>

                    </Row>
                </Grid>
            </div>
        );
    }
}

export default AdsList;
