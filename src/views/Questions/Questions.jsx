import React, {Component} from 'react';
import {Col, ControlLabel, FieldGroup, FormControl, FormGroup, Grid, InputGroup, Row} from 'react-bootstrap';
import Checkbox from 'elements/CustomCheckbox/CustomCheckbox';
import {FormInputs} from 'components/FormInputs/FormInputs.jsx';
import {UserCard} from 'components/UserCard/UserCard.jsx';
import Button from 'elements/CustomButton/CustomButton.jsx';
import {addTag, createQuestion, getCategories, getTags} from "../../operations";
import NotificationSystem from "react-notification-system";
import Card from "../../components/Card/Card";
import {createCategory} from "../../operations/index";
import {NavLink} from "react-router-dom";
import Slider, {createSliderWithTooltip} from "rc-slider";
import 'rc-slider/assets/index.css';
import { WithContext as ReactTags } from "react-tag-input";
import '../../assets/css/tag_styles.css';

const DifficultySlider = createSliderWithTooltip(Slider);


const KeyCodes = {
    comma: 188,
    enter: 13,
};

const delimiters = [KeyCodes.comma, KeyCodes.enter];

class AddQuestion extends Component {

    createQuestionLocal = () => {
        this.setState({spin: "fa fa-spinner fa-spin"});
        createQuestion({
            "question": this.inputQuestion.value,
            "choices": [
                {"identifier": "A", "option": this.inputChoiceA.value},
                {"identifier": "B", "option": this.inputChoiceB.value},
                {"identifier": "C", "option": this.inputChoiceC.value},
            ],
            "correctAnswer": this.inputCorrectAnswer.value,
            "difficulty": this.state.difficulty,
            "explanation": this.inputExplanation.value,
            "asked_games": [],
            "categories": this.state.checkedCategories,
            "tags": this.state.tags
        }).then((response) => {
            this.setState({spin: ""});
            this.addNotification(`Question "${response.question}" successfully created`)
        });
    };

    createCategoryLocal = (text) => {
        createCategory({
            name: text
        }).then(response => {
            this.addNotification(`Category "${response.name}" Successfully created with id ${response._id}`);
            this.setState({
                categories: this.state.categories.concat(response),
                toggleAddCategory: false
            })
        })
    };

    addNotification = (message) => {
        this._notificationSystem.addNotification({
            message: message,
            level: 'success',
            position: "tc"
        })
    };

    onCheckboxChange = (isChecked, cat) => {
        if (typeof isChecked !== "undefined") {
            alert(isChecked);
            if (!isChecked) {
                this.setState({
                    checkedCategories: this.state.checkedCategories.concat(cat)
                })
            } else {
                this.setState({
                    checkedCategories:
                        this.state.checkedCategories.filter((c) => {
                            return c._id !== cat._id
                        })
                })
            }
        }
    };

    constructor(props) {
        super(props);
        this.state = {
            question: "",
            choiceA: "",
            choiceB: "",
            choiceC: "",
            correctAnswer: "",
            duration: "",
            spin: "",
            categories: [],
            toggleAddCategory: false,
            checkedCategories: [],
            difficulty: 1,
            tags: [],
            suggestions: [],
        };


        this.handleDelete = this.handleDelete.bind(this);
        this.handleAddition = this.handleAddition.bind(this);
        this.handleDrag = this.handleDrag.bind(this);
        this.handleTagClick = this.handleTagClick.bind(this);
    }

    handleDelete(i) {
        const {tags} = this.state;
        this.setState({
            tags: tags.filter((tag, index) => index !== i),
        });
    }

    handleAddition(tag) {
        addTag(tag);
        this.setState(state => ({tags: [...state.tags, tag]}));
    }

    handleDrag(tag, currPos, newPos) {
        const tags = [...this.state.tags];
        const newTags = tags.slice();

        newTags.splice(currPos, 1);
        newTags.splice(newPos, 0, tag);

        // re-render
        this.setState({tags: newTags});
    }

    handleTagClick(index) {
        console.log('The tag at index ' + index + ' was clicked');
    }

    componentDidMount() {

        this._notificationSystem = this.refs.notificationSystem;

        getCategories().then(categories => this.setState({categories: categories.data}));

        getTags().then(tags => this.setState({suggestions: tags.data}));
    }


    render() {
        console.log(this.props);
        return (
            <div className="content">
                <div style={{marginLeft: 20}}>
                    <NavLink to={"/see_questions"} className="nav-link">
                        <p>{"See Questions"}</p>
                    </NavLink>
                </div>
                <Grid fluid>
                    <Row>
                        <Col md={8}>
                            <Card
                                title="Add Question"
                                content={
                                    <form>
                                        <FormGroup>
                                            <ControlLabel>Questions</ControlLabel>
                                            <FormControl id="questions"
                                                         type="text"
                                                         placeholder="Enter Question..."
                                                         inputRef={(input) => this.inputQuestion = input}/>
                                        </FormGroup>

                                        <Card
                                            content={
                                                <Col>
                                                    <ControlLabel>Choices</ControlLabel>
                                                    <InputGroup className="col-md-12">
                                                        <InputGroup.Addon
                                                            style={{backgroundColor: "#F1F1F1"}}>A. </InputGroup.Addon>
                                                        <FormControl
                                                            id="choiceA"
                                                            type="text"
                                                            placeholder="Choice A"
                                                            inputRef={(input) => this.inputChoiceA = input}
                                                        />
                                                    </InputGroup>

                                                    <InputGroup className="col-md-12">
                                                        <InputGroup.Addon
                                                            style={{backgroundColor: "#F1F1F1"}}>B. </InputGroup.Addon>
                                                        <FormControl
                                                            id="choiceB"
                                                            type="text"
                                                            placeholder="Choice B"
                                                            inputRef={(input) => this.inputChoiceB = input}
                                                        />
                                                    </InputGroup>

                                                    <InputGroup className="col-md-12">
                                                        <InputGroup.Addon
                                                            style={{backgroundColor: "#F1F1F1"}}>C. </InputGroup.Addon>
                                                        <FormControl
                                                            id="choiceC"
                                                            type="text"
                                                            placeholder="Choice C"
                                                            inputRef={(input) => this.inputChoiceC = input}
                                                        />
                                                    </InputGroup>

                                                    <div/>
                                                </Col>
                                            }
                                        />

                                        <Card
                                            content={
                                                <Col>

                                                    <FormGroup>
                                                        <ControlLabel>Correct Answer</ControlLabel>
                                                        <FormControl type="password"
                                                                     bsClass="form-control"
                                                                     placeholder="Letter"
                                                                     inputRef={(input) => this.inputCorrectAnswer = input}/>
                                                    </FormGroup>

                                                    <FormGroup>
                                                        <ControlLabel>Explanation</ControlLabel>
                                                        <FormControl type="text"
                                                                     bsClass="form-control"
                                                                     placeholder="Explanation"
                                                                     inputRef={(input) => this.inputExplanation = input}/>
                                                    </FormGroup>

                                                    <FormGroup>
                                                        <ControlLabel>Difficulty Slider</ControlLabel>
                                                        {/*<FormControl type="number"
                                                                     bsClass="form-control"
                                                                     placeholder="Duration in sec"
                                                                     inputRef={(input) => this.inputDuration = input}/>*/}
                                                        <DifficultySlider
                                                            style={{
                                                                width: "99%",
                                                                marginTop: 10,
                                                                marginLeft: "1%",
                                                            }}
                                                            min={1}
                                                            max={10}
                                                            defaultValue={1}
                                                            vertical={false}
                                                            marks={
                                                                {
                                                                    1: "1",
                                                                    2: "2",
                                                                    3: "3",
                                                                    4: "4",
                                                                    5: "5",
                                                                    6: "6",
                                                                    7: "7",
                                                                    8: "8",
                                                                    9: "9",
                                                                    10: "10",
                                                                }
                                                            }
                                                            onChange={(value) => {
                                                                this.setState({
                                                                    difficulty:value
                                                                })
                                                            }}
                                                        />
                                                    </FormGroup>
                                                </Col>

                                            }
                                        />

                                        <Button
                                            onClick={(e) => this.createQuestionLocal()}
                                            bsStyle="info"
                                            pullRight
                                            fill>
                                            <i className={this.state.spin}/>
                                            Add Question
                                        </Button>
                                        <div className="clearfix"/>
                                    </form>
                                }
                            />
                        </Col>
                        <Col md={4}>

                            <Card
                                title="Tags"
                                content={
                                    <div>
                                        <ReactTags
                                            tags={this.state.tags}
                                            suggestions={this.state.suggestions}
                                            delimiters={delimiters}
                                            handleDelete={this.handleDelete}
                                            handleAddition={this.handleAddition}
                                            handleDrag={this.handleDrag}
                                            handleTagClick={this.handleTagClick}
                                        />
                                    </div>
                                }
                            />

                            <Card
                                title="Category"
                                content={
                                    <div>
                                        {
                                            this.state.categories.map((c, i) => {
                                                return (
                                                    <Checkbox
                                                        id={c._id}
                                                        label={c.name}
                                                        onStateChange={(isChecked) => this.onCheckboxChange(isChecked, c)}
                                                    />)
                                            })
                                        }
                                        <Button
                                            onClick={(e) => this.setState({toggleAddCategory: !this.state.toggleAddCategory})}
                                            bsStyle="link" style={{borderWidth: 0}}>Add new Category</Button>

                                        {
                                            (this.state.toggleAddCategory) ?
                                                <Card
                                                    content={
                                                        <div>
                                                            <FormControl type="text"
                                                                         bsClass="form-control"
                                                                         placeholder="Category Name"
                                                                         inputRef={(input) => this.inputCategoryName = input}/>
                                                            <Button bsStyle="link" style={{borderWidth: 0}}
                                                                    onClick={(e) => this.createCategoryLocal(this.inputCategoryName.value)}>Add</Button>
                                                        </div>
                                                    }
                                                /> : <div/>
                                        }
                                    </div>
                                }
                            />
                        </Col>

                    </Row>
                </Grid>
                <NotificationSystem ref="notificationSystem"/>
            </div>
        );
    }
}

export default AddQuestion;
