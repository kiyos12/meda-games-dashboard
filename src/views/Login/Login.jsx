import React, {Component} from 'react';
import {FormControl, InputGroup} from "react-bootstrap";
import Button from 'elements/CustomButton/CustomButton.jsx';
import {authenticate} from "../../api/index";
import NotificationSystem from "react-notification-system";
import {setAuthenticated} from "../../operations/index";

class Login extends Component {

    _notificationSystem = null;
    addNotification = (message, status) => {
        this._notificationSystem.addNotification({
            message: message,
            level: status,
            position: "tc"
        })
    };
    login = () => {
        this.setState({spin: "fa fa-spinner fa-spin"});
        authenticate(this.inputEmail.value, this.inputPassword.value)
            .then(res => {
                this.setState({spin: ""});
                setAuthenticated(true);
                this.addNotification("Successful Login !!!", "success");
                this.props.history.push("/dashboard")
            })
            .catch(err => {
                this.setState({spin: ""});
                this.addNotification("Login Error " + err.message, "error")
            });
    };

    constructor(props) {
        super(props);
        this.state = {
            spin: ""
        }
    }

    componentDidMount() {
        this._notificationSystem = this.refs.notificationSystem;
        authenticate()
            .then(res => {
                setAuthenticated(true);
                this.props.history.push(this.props.location.state.from.pathname);
            })
    }

    render() {

        return (
            <div style={styles.loginWrapper} data-color={"blue"}>
                <NotificationSystem ref="notificationSystem"/>
                <div style={styles.mainCard}>
                    <img style={styles.icon} src={require('../../assets/img/favicon.png')}/>
                    <h2 style={styles.title}>Login : Meda ሺ</h2>
                    <div style={{justifyContent: "center"}}>
                        <InputGroup className="col-md-12" style={{marginBottom: 20}}>
                            <InputGroup.Addon
                                style={{backgroundColor: "#F1F1F1"}}>
                                <i className="pe-7s-user"
                                   style={{width: 20, height: 20}}/>
                            </InputGroup.Addon>
                            <FormControl
                                id="username"
                                type="text"
                                placeholder="Email"
                                inputRef={(input) => this.inputEmail = input}
                            />
                        </InputGroup>

                        <InputGroup className="col-md-12">
                            <InputGroup.Addon
                                style={{backgroundColor: "#F1F1F1"}}><i className="pe-7s-key"
                                                                        style={{width: 20, height: 20}}/>
                            </InputGroup.Addon>
                            <FormControl
                                id="password"
                                type="password"
                                placeholder="Password"
                                inputRef={(input) => this.inputPassword = input}
                            />
                        </InputGroup>

                        <Button
                            onClick={(e) => this.login()}
                            style={{marginTop: 20, width: "100%"}}
                            bsStyle="info"
                            fill>
                            Login
                            <i className={this.state.spin} style={{marginLeft: "5%"}}/>
                        </Button>
                    </div>
                </div>
            </div>
        );
    }
}

const styles = {
    loginWrapper: {
        display: 'flex',
        flex: 1,
        width: "100%",
        height: "100%",
        justifyContent: "center",
        alignItems: "center",
        background: `linear-gradient(rgba(61, 124, 196, 0.9), rgba(61, 124, 196, 0.8)),url(${require("../../assets/img/login_bg.jpg")}) no-repeat`,
        backgroundSize: "cover"
    },
    mainCard: {
        backgroundColor: "#FFF",
        display: "flex",
        width: "30%",
        padding: 20,
        flexDirection: 'column',
        borderRadius: 10
    },
    icon: {
        width: "18%",
        height: "10%",
        marginTop: "-50px",
        alignSelf: 'center',
    },
    title: {
        alignSelf: 'center',
        color: "#A9A9A9"
    }
};

Login.propTypes = {};
Login.defaultProps = {};

export default Login;
