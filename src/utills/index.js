import NotificationSystem from "react-notification-system";
import React, {Component} from 'react';

class Notification extends Component {

    _notificationSystem = null;

    static addNotification = (event, message) => {
        event.preventDefault();
        this._notificationSystem.addNotification({
            message: message,
            level: 'success'
        })
    };

    componentDidMount() {
        this._notificationSystem = this.refs.notificationSystem;
    }

    render() {
        return (
            <div>
                <NotificationSystem ref="notificationSystem"/>
            </div>
        );
    }
}

Notification.propTypes = {};
Notification.defaultProps = {};

export default Notification;
