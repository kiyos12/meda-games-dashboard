export const countDown = (date, callback) => {

    let getTimeFromDate = new Date(date).getTime();

    const interval = setInterval(() => {

        let now = new Date().getTime();

        let dateInterval = (date) = getTimeFromDate - now;

        let days = Math.floor(dateInterval / (1000 * 60 * 60 * 24));

        let hours = Math.floor((dateInterval % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));

        let minutes = Math.floor((dateInterval % (1000 * 60 * 60)) / (1000 * 60));

        let seconds = Math.floor((dateInterval % (1000 * 60)) / 1000);

        callback(days, hours, minutes, seconds);

        if (dateInterval < 0) {
            clearInterval(interval);
        }

    }, 1000)
};

export const getUTCOfDate = (dateString) => {
    let date = new Date(dateString);
    return date.getUTCFullYear() + "-" + date.getUTCMonth() + "-" + date.getUTCDate()
}



